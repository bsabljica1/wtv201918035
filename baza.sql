SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


CREATE TABLE `adresar` (
  `idKontakta` int(11) NOT NULL,
  `idPoznanik` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `imenik` (
  `id` int(11) NOT NULL,
  `ime` varchar(30) NOT NULL,
  `prezime` varchar(30) NOT NULL,
  `adresa` varchar(30) NOT NULL,
  `br_telefona` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


INSERT INTO `imenik` (`id`, `ime`, `prezime`, `adresa`, `br_telefona`) VALUES
(1, 'ime1', 'prezime1', 'adresa1', '061/111-111'),
(2, 'ime2', 'prezime2', 'adresa2', '061/222-222'),
(3, 'ime3', 'prezime3', 'adresa3', '061/333-333');


ALTER TABLE `imenik`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `imenik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;
