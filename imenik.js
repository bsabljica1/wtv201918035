const Sequelize = require("sequelize");
const sequelize = require("./baza.js");
const Imenik = sequelize.define('Imenik', {
   ime: Sequelize.STRING,
   prezime: Sequelize.STRING,
   adresa: Sequelize.STRING,
   brojTelefona: {
       type: Sequelize.STRING,
       validate: {
           is: {
               args: [/[0-9]{3}\/[0-9]{3}\-[0-9]{3}/],
               msg: "Nije pravilan format broja telefona"
           }
       }
   },
   datumDodavanja: Sequelize.DATE
})
module.exports = function (sequelize, DataTypes) {
   return Imenik;
}
